#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


HARFILE=$DIR/test.har
RESULT=0

#set -ex

# ----------------------------------------

echo 'Running har-recorder test'

echo Test har file location: $HARFILE
rm -f $HARFILE

gl-har-recorder start -p 8080 -o $HARFILE
rc=$?
if [[ $rc -ne 0 ]] ; then
  echo 'Error couldnt start recorder'
  RESULT=1
fi

curl --proxy http://127.0.0.1:8080 -k "https://gitlab.com/api/v4/projects/gitlab-org%2Fsecurity-products%2Fhar-recorder"
rc=$?
if [[ $rc -ne 0 ]] ; then
  echo 'Error curl failed'
  RESULT=1
fi

gl-har-recorder stop
rc=$?
if [[ $rc -ne 0 ]] ; then
  echo 'Error stopping recorder'
  RESULT=1
fi

if [[ ! -e "$HARFILE" ]]; then
  echo "HAR file doesn't exist"
  RESULT=1
fi

if [[ "$(jq '.log.entries | length' $HARFILE)" != "1" ]] ; then
  echo "Wrong number of entries in HAR file"
  RESULT=1
fi

if [[ "$(jq '.log.entries| .[0] | .request.url' $HARFILE)" != "\"https://gitlab.com/api/v4/projects/gitlab-org%2Fsecurity-products%2Fhar-recorder\"" ]]; then
  echo "Wrong URL in first entry"
  RESULT=1
fi

rm -f $HARFILE

# ----------------------------------------

if [[ $RESULT -ne 0 ]] ; then
    echo 'Some recorder tests failed.'; exit $RESULT
fi

echo "All recorder tests passed!"

# end
